# N1.pdf

#n.1.1

print ( " ja e meu segundo codigo ")

#n.1.2

print ( " cadastrar\n logar\n sair ")

#n.1.3

for numeros_pares in range (2, 11, 2):
	print (numeros_pares)



# N2.pdf

# n.2.1

quantidade_cebolas = 10

preco_cebolas = 2

print ( " voce ira pagar ", quantidade_cebolas * preco_cebolas, " pelas cebolas " )

# n2.2

# atribui valor inteiro a cabolas (variavel)
cebolas = 300

# atribui valor inteiro a cabolas_na_caixa (variavel)
cebolas_na_caixa = 120

# atribui valor inteiro a espaco_caixa (variavel)
espaco_caixa = 5

# atribui valor inteiro a caixas (variavel)
caixas = 60

# atribui a variável cabolas_fora_da_caixa o resultado da subtração entre cebolas e cebolas_na_caixa
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa

# atribui a variável caixas_vazias o resultado da subtração entre caixas pela divisão de cebolas_na_caixa por espaco_caixa
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa)

# atribui a variável caixas_necessarias o resultado da divisão entre cebolas_fora_da_caixa e espaco_caixa
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa

# imprime caracteres junto com o valor armazenado na variável (cebolas_na_caixa)
print("Existem", cebolas_na_caixa, "cebolas encaixotadas")
# imprime caracteres junto com o valor armazenado na variável (cebolas_fora_da_caixa)
print("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")
# imprime caracteres junto com o valor armazenado na variável (espaco_caixa)
print("Em cada caixa cabem", espaco_caixa, "cebolas")
# imprime caracteres junto com o valor armazenado na variável (caixas_vazias)
print("Ainda temos,", caixas_vazias, "caixas vazias")
# imprime caracteres junto com o valor armazenado na variável (caixas_necessarias)
print("Então, precisamos de", caixas_necessarias, "caixas para empacotar todas as cebolas")

#n.2.3

cebolas = 24322
cebolas_na_caixa = 978
espaco_caixa = 3
caixas = 200
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa)
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa

print("Existem", cebolas_na_caixa, "cebolas encaixotadas")
print("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")
print("Em cada caixa cabem", espaco_caixa, "cebolas")
print("Ainda temos,", caixas_vazias, "caixas vazias")
print("Então, precisamos de", caixas_necessarias, "caixas para empacotar todas as cebolas")


# N4.pdf

#n4.1

name = ( " \nDaniel\t " )
print ( name.rstrip() + name.lstrip() + name.strip ())

#n4.2

name = input ( " digite uma frase qualquer: " )
print (name.replace("",""))


#N5.pdf

#n5.1

numero = int(input("digite o numero"))
r = numero % 2
if r == 0:
	print(str(numero)+"numero par")

else:
	print(str((numero)+ "numero impar"))

#n5.2

numero = int(input("digite o numero"))
r = numero % 2

if not checkprime(numero):
	numero = "nao primo"

else:
	numero = "primo"

#n5.3

d = int(input(" digite o primeiro numero "))
i = int(input(" digite o segundo numero "))

print("adicionar:", d+i)
print("subtrair:", d-i)
print("multiplicar:", d*i)
print("dividir:", d/i)

#n5.4

idade = int(input("sua idade"))
semestre = int(input("semestre em que se encontra"))

s = (8 - semestre)/2

print("voce se forma em", s, "quanto anos falta", idade + s, "idade")

#n5.5

idade = int(input("sua idade"))
semestre = int(input("semestre em que se encontra"))
atraso = float(input("semestres atrasados"))

s = (8 - semestre)/2

print("voce forma em",8-(semestre-atraso))/2, "ano/semestre"

#N6.pdf

#n6.1

nomes = ("Diego", "Samuel", "Breno", "Gabriel")
for i in nomes:
	print(i)

#n6.2 e n.6.3

nomes = ("Diego", "Samuel", "Breno", "Gabriel")
for i in nomes:
	print(i)

print("eu gostaria de comprar um carro e uma moto")


#n7.pdf

#n7.1

nomes = ("Diego", "Samuel", "Breno", "Gabriel")
for i in nomes:
	print("voce foi convidado para jantar",i)

#n7.2

def new(list):
	for i in list:
		print("vcoe foi convidado para jantar")

names = ("Diego", "Samuel", "Breno", "Gabriel")

print("essa pessoa", names(3), "nao irar vir")

names.ausente("breno")

#n7.3

def new(list):
	for i in list:
		print("vcoe foi convidado para jantar")


names = ("Diego", "Samuel", "Breno", "Gabriel")

print("essa pessoa", names(3), "nao irar vir")

names.ausente("breno")

print("uma mesa maior foi disponobilizada para o jantar\n")

names.insert("Bruna")
names.insert("Marco")
names.append("Guilherme")

#N8.pdf

places = ("Roma", "Grecia", "Nova York", "Dubai", "China")

print(places)
print(sorted(places))

print(places)
print(places.reverse())
print(places.reverse())
print(places.sort())
print(places.reverse())


#N9.pdf

#n9.1

lista = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
sum = sum(lista)

print(sum)
for i in lista:
	resultado = resultado + i

#n9.2

lista = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
sum = sum(lista)

print(sum)
for i in lista:
	resultado = resultado * i

#n9.3

def menor_nume(lista):
	lista.sorteio()
	return lista()

lista = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

#n9.4

def itens_iguais(lista, listab):
	return lista(set(lista).intersection(listab))

#m9.5

lista = ("cdb", "abc", "ezf")

lista.sort()

print(lista)

#n9.6

def value_size(x):
	value = ()
	for x in list:
		value.append(x)
	return value


#N10.pdf

#n10.1

for numero in range(1, 30):
	print(numero)

#n10.2

lista ()
numb = (lista for lista in range(1, 1000001))
print(lista)

#n10.3

lista ()
numb = (lista for lista in range(1, 1000001))

print(min(lista))
print(max(lista))
print(sum(lista))

#n10.4

lista()
for x in range(1, 20, 2 ):
	print(x)

#n10.5

multiplos = (numb for numb in range(3, 1000, 3))
for numb in multiplos:

#n10.6

#n10.7


#N11.pdf

#n11.1

#n11.2

#n11.3


#n12.pdf